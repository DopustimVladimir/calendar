
addEventListener("DOMContentLoaded", function() {

    const darkModeCheckbox = document.getElementById("darkmode")

    function updateTheme() {
        darkModeCheckbox.checked
            ? document.body.classList.add("dark-mode")
            : document.body.classList.remove("dark-mode")
    }
    darkModeCheckbox.addEventListener("change", function() {
        updateTheme()
    })
    updateTheme()
})
